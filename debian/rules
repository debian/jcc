#!/usr/bin/make -f

export DEB_BUILD_MAINT_OPTIONS = hardening=+all
DPKG_EXPORT_BUILDFLAGS = 1
include /usr/share/dpkg/buildflags.mk

export DH_VERBOSE = 1
export PYBUILD_NAME = jcc

include /usr/share/dpkg/architecture.mk

JAVAARCH :=$(DEB_HOST_ARCH_CPU)

ifeq ($(DEB_HOST_ARCH_CPU),powerpc)
  JAVAARCH :=ppc
endif

ifeq ($(DEB_HOST_ARCH_CPU),sh4)
  JAVAARCH :=sh
endif

ifeq ($(DEB_HOST_ARCH_CPU),arm64)
  JAVAARCH :=aarch64
endif

ifeq ($(DEB_HOST_ARCH_CPU),ppc64el)
  JAVAARCH :=ppc64le
endif

ifneq ($(wildcard /usr/lib/jvm/default-java/jre),)
    JAVA_LIB_PATH := /usr/lib/jvm/default-java/jre/lib/$(JAVAARCH)
else
    JAVA_LIB_PATH := /usr/lib/jvm/default-java/lib
endif

# Expand the default-java symlink, so the runtime dependecy is on the correct
# JDK version.
JAVA_LIB_PATH := $(realpath $(JAVA_LIB_PATH))

# Use the server mode JVM, but if not available fall back to client mode
ifneq ($(wildcard $(JAVA_LIB_PATH)/server/libjvm.so),)
    JVM_LIB_PATH := $(JAVA_LIB_PATH)/server
else
    JVM_LIB_PATH := $(JAVA_LIB_PATH)/client
endif

JRE_PACKAGE := $(shell dpkg -S $(JVM_LIB_PATH)/libjvm.so | cut -d : -f 1)

export JCC_ARGSEP=;
export JCC_LFLAGS := -L$(JVM_LIB_PATH);-ljvm;-Wl,-rpath=$(JVM_LIB_PATH)

export JCC_CFLAGS := -fdollars-in-identifiers
export JCC_JDK := /usr/lib/jvm/default-java

# For shared mode we need patch http://bugs.python.org/setuptools/issue43 for setuptools to be applied
export NO_SHARED=1

%:
	dh $@ --with python3 --buildsystem=pybuild

override_dh_auto_install:
	dh_auto_install
	rm -rf debian/jcc/usr/share/pyshared/jcc/patches

override_dh_gencontrol:
	dh_gencontrol -- -Vjre:Depends="$(JRE_PACKAGE)"

override_dh_strip:
	# _jcc.so does not seem to contain the debug section, so disable
	# building of a debug package, for now.
	dh_strip --no-automatic-dbgsym

override_dh_dwz:
	# Ignore this step because *.so does not have debug.section
	true

override_dh_auto_clean:
	# dh_auto_clean
	rm -rf build/*
	rm -f jcc/config.py
